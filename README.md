# DouraSoft

Desafio Programador PHP - DouraSoft

Desenvolvimento de um CRUD de cadastro de produtos em PHP e PostgreSQL

## Deverá conter
Produtos: ID, Codigo, Nome, Descrição e Preço

Clientes: ID, Nome, Telefone, Endereço

Pedidos: ID, Cliente, Total, Data, Produtos

## Instruções

1. Faça um fork do projeto para sua conta pessoal
2. Crie uma branch com o padrão: `desafio-seu-nome`
3. Coloque seu teste dentro do diretório `/desafio-php/seu-nome`
4. Utilize o seu nome para a pasta principal do projeto
5. Submeta seu código criando um Pull Request

## Você pode

- Utilizar qualquer Framework. Caso opte por não utilizar, desenvolver nos padrões de projeto MVC
- Utilizar composer
- Utilizar qualquer template engine de sua preferência
- Utilizar quaisquer bibliotecas ou frameworks JS como Angular, React, jQuery, VueJS ou outras
- Utilizar bibliotecas CSS como Compass, Bourbon, Animatecss ou outras
- Utilizar quaisquer frameworks CSS como Bootstrap, Foundation ou outras

## Você deve

- Disponibilizar o SQL em uma pasta do projeto
- Escrever um README.md que explique como instalar seu projeto

## Dúvidas?

Abra uma [issue](https://bitbucket.org/paulo-dourasoft/dourasoft/issues/new)

Boa sorte! :)
